##TO RUN:
## %> . venv/bin/activate
## %>  python brew_server.py
##

#imports
from flask import Flask, url_for, render_template, request, session, g, redirect, abort, flash
import sqlite3

#config
DATABASE='brew_server.sql'
DEBUG=True
SECRET_KEY='key'
USERNAME='esears'
PASSWORD='esears'

app = Flask(__name__)
# can put the config into a file
#BREW_SERVER_SETTINGS= <filename>
#app.config.from_envvar('BREW_SERVER_SETTINGS', silent=True)
app.config.from_object(__name__)

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
      db.close()

@app.route('/')
@app.route('/index.html')
def serve_page_index():
    return render_template('index.html',)

@app.route('/brewing/on_tap.html')
def serve_page_brewing_on_tap():
    cur = g.db.execute("SELECT * FROM brews WHERE on_tap=1")
    for row in cur.fetchall():
      brew=row

    #check if db entry exists
    if 'brew' in locals():
      cmd = "SELECT * FROM keg where name=\'"+brew[0]+"\'"
      print cmd+"\n"
      cur = g.db.execute(cmd)
      for row in cur.fetchall():
        keg_stats=row
    else:
      #keg: [name,current_volume,total_volume]
      keg_stats = ["None",0,0]
    return render_template('brewing/on_tap.html',name=keg_stats[0],current_volume=keg_stats[1],total_volume=keg_stats[2])

@app.route('/brewing/fermenting.html')
def serve_page_brewing_fermenting():
    cur = g.db.execute("SELECT * FROM brews WHERE fermenting=1")
    for row in cur.fetchall():
      brew_name=row
    #check if db entry exists
    if 'brew_name' in locals():
      return render_template('brewing/fermenting.html', name=brew_name[0])
    else:
      return render_template('brewing/fermenting_none.html', )

@app.route('/brewing/brews.html')
def serve_page_brewing_brews():
    return render_template('brewing/brews.html',)

@app.route('/brewing/brew_tech.html')
def serve_page_brewing_brew_tech():
    return render_template('brewing/brew_tech.html',)

#to update db:
#http://127.0.0.1:5000/brewing/use_keg.html?dispensed=1
@app.route('/brewing/use_keg.html',methods=['GET'])
def serve_page_brewing_use_keg():
    cur = g.db.execute("SELECT name FROM brews WHERE on_tap=1")
    for row in cur.fetchall():
      brew=row
    brew_name = brew[0]
    cmd = "SELECT current_volume FROM keg where name=\'"+brew_name+"\'"
    cur = g.db.execute(cmd)
    for row in cur.fetchall():
      keg=row
    current_volume = int(keg[0])
    new_volume = current_volume - int(request.args.get('dispensed'))
    g.db.execute('update keg set current_volume=? where name=?',[new_volume,brew_name])
    g.db.commit()
    return render_template('index.html',)

@app.route('/brewing/add_brew.html',methods=['GET', 'POST'])
def serve_page_brewing_add_brew():
    if not session.get('logged_in'):
      return render_template('brewing/login.html',)
    else:
      if request.method == 'GET':
        null=1; ##do nothing
      elif request.method == 'POST':
        g.db.execute('insert into brews (name, style, brew_date, in_bottles, on_tap, fermenting) values (?, ?, ?, ?, ?, ?)',[request.form['brew-name'], request.form['brew-type'],request.form['brew-date'],'is_in_bottles' in request.form,'is_on_tap' in request.form,'is_fermenting' in request.form])

        #instert into keg table...
        if (('is_on_tap' in request.form) == 1):
          #calculate beers from volume. Gal*128 = oz
          total_beers = int((float(request.form['vol_into_fermenter'])*128)/16)
          g.db.execute('insert into keg (name, current_volume, total_volume) values (?, ?, ?)',[request.form['brew-name'], total_beers,total_beers])

        #instert into bottles table... TODO
        #if (('is_in_bottles' in request.form) == 1):
          #g.db.execute('insert into bottles (name, num_bottles_12oz, num_bottles_22oz) values (?, ?, ?)',[request.form['brew-name'], request.form[''],request.form['']])

        #instert into grain table
        process_grain_fields=1
        grain_field_num=1
        while (process_grain_fields==1):
          grain_type_field = "grain"+str(grain_field_num)+"_type"
          grain_amount_field = "grain"+str(grain_field_num)+"_amount"
          if ((grain_type_field in request.form) == 1):
            g.db.execute('insert into grain (name, type, amount) values (?, ?, ?)',[request.form['brew-name'], request.form[grain_type_field],request.form[grain_type_field]])
          else:
            process_grain_fields=0

        #instert into hops table
        process_hop_fields=1
        hop_field_num=1
        while (process_hop_fields==1):
          hop_type_field = "hop"+str(hop_field_num)+"_type"
          hop_amount_field = "hop"+str(hop_field_num)+"_amount"
          hop_minutes_field = "hop"+str(hop_field_num)+"_mins"
          if ((hop_type_field in request.form) == 1):
            g.db.execute('insert into hops (name, type, amount, minutes) values (?, ?, ?, ?)',[request.form['brew-name'], request.form[hop_type_field],request.form[hop_type_field],request.form[hop_minutes_field]])
          else:
            process_hop_fields=0

        #instert into yeast table
        g.db.execute('insert into yeast (name, type, starter_vol, starter_dme, vol_pitched) values (?, ?, ?, ?, ?)',[request.form['brew-name'], request.form['yeast_type'],request.form['starter_vol'],request.form['starter_dme'],request.form['vol_pitched']])

        #instert into mash table
        g.db.execute('insert into mash (name, pre_boil_vol, strike_temp, mash_temp, mash_time, OG, FG, ABV, vol_into_fermenter) values (?, ?, ?, ?, ?, ?, ?, ?, ?)',[request.form['brew-name'], request.form['pre_boil_vol'],request.form['strike_temp'],request.form['mash_temp'],request.form['mash_time'],request.form['OG'],request.form['FG'],request.form['ABV'],request.form['vol_into_fermenter']])

        g.db.commit()
        flash('New entry was successfully posted')
      return render_template('brewing/add_brew.html',)

@app.route('/brewing/login.html', methods=['GET', 'POST'])
def serve_page_brewing_login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
            flash('Invalid username')
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
            flash('Invalid password')
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('serve_page_brewing_add_brew'))
    return render_template('brewing/login.html', error=error)

@app.route('/brewing/logout')
def brewing_logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('serve_page_brewing_login'))

#to run the application
if __name__ == '__main__':
    app.run(debug=DEBUG)
